import { Component, OnInit } from '@angular/core';
import { CalendarModule } from 'ion2-calendar';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.page.html',
  styleUrls: ['./calendario.page.scss'],
})
export class CalendarioPage implements OnInit {
  listTareas: any = [
    {
      asunto: 'Cita con la gerencia financiera',
      date: 'Diciembre 02 de 2019',
      hora: '10:00 am',
      nombreAsignador: 'Claudia Cifuentes'
    },
    {
      asunto: 'Cita con la gerencia administrativa',
      date: 'Diciembre 02 de 2019',
      hora: '10:30 am',
      nombreAsignador: 'Claudia Cifuentes'
    },
    {
      asunto: 'Cita con la gerencia general',
      date: 'Abril 03 de 2019',
      hora: '11:00 am',
      nombreAsignador: 'Claudia Cifuentes'
    },
    {
      asunto: 'Cita con la gerencia financiera',
      date: 'Diciembre 03 de 2019',
      hora: '11:30 am',
      nombreAsignador: 'Claudia Cifuentes'
    },
    {
      asunto: 'Cita con la gerencia financiera',
      date: 'Diciembre 10 de 2019',
      hora: '12:00 pm',
      nombreAsignador: 'Claudia Cifuentes'
    },
    {
      asunto: 'Cita con la gerencia financiera',
      date: 'Diciembre 15 de 2019',
      hora: '3:00 pm',
      nombreAsignador: 'Claudia Cifuentes'
    },
    {
      asunto: 'Cita con la gerencia financiera',
      date: 'Diciembre 17 de 2019',
      hora: '3:00pm',
      nombreAsignador: 'Claudia Cifuentes'
    }
  ];

  date: string;
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  constructor(
    private calendarModule: CalendarModule
  ) { }

  ngOnInit() {
  }

  onChange($event) {
    console.log($event);
  }
}
